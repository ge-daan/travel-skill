#This class is a skill that can be used for the mycroft bot. It uses the NS-API "reisinformatie API". 
#The bot understands the user input via the intent_handler function and the voc files that are stated behind teh function.
#It will do a API request with the given information from the user and if it is missing information ask the user to give the missing information.
#If there are no problems the bot will respond with the time and platfrom of the train journey.
#If there are any problems the bot wil try to fix it by asking the user for extra information and if that doesn't fix it wil eventially respond,
#that it didn't understand the user input. 

from adapt.intent import IntentBuilder
from mycroft.util.format import (nice_date, nice_duration, nice_time,
                                 date_time_format)
from mycroft.util.time import now_utc, to_local, now_local
from mycroft.skills.core import MycroftSkill, intent_handler
from mycroft.util.log import LOG
import http.client, urllib.request, urllib.parse, urllib.error, base64, json, re
from datetime import datetime, time, timezone
import pytz
from enum import Enum

class MissingInfoType(Enum):
    departure_station = 1
    arrival_station = 2

   
class TravelSkill(MycroftSkill):

    def __init__(self):
        super(TravelSkill, self).__init__(name="TravelSkill")
        self.attempts = 0
        self.platform = '' 

    #Get user input for API-request and if data if correct respond with time and platform of the train journey.
    #If there is a problem with the API-request or user input and couldn't solve it by asking follow up questions,
    #it will respond that it didn't understand the question
    @intent_handler(IntentBuilder('').require('Query').require('Travel').optionally('departure')
    .optionally('arrival').optionally('time'))
    def handle_travel_advice(self, message):
        utt = message.data.get('utterance', "").lower()
        files = ['arrival', 'departure', 'time']
        entities = {'arrival': '', 'departure': '','time': ''}

        for x in files:
            location = self.extract_regex_info(utt, x)
            if location == None:
                entities[x] == None
            entities[x] = location
            
        departure_time = self.get_departure_time(entities['arrival'], entities['departure'], entities['time'])

        if departure_time == None:
            self.speak_dialog("Sorry I don't understand the question with the given information")
            self.attempts = 0       
        else:
             self.speak_dialog('The train will leave at {} from platform {}'.format(departure_time, self.platform))
             self.attempts = 0 
 
    #extract info using regex files for departure station, arrival station and time
    def extract_regex_info(self, utt, file_name):
        rx_file = self.find_resource('{}.rx'.format(file_name), 'regex')
        if rx_file:
            with open(rx_file) as f:
                for pat in f.read().splitlines():
                    pat = pat.strip()
                    if pat and pat[0] == "#":
                        continue
                    res = re.search(pat, utt)
                    if res:
                        try:
                            return res.group('{}'.format(file_name))
                        except IndexError:
                            pass
        return None

    #If there is missing information of the API gave a bad request code it will use this function to try to get the wright information
    #It will return the wright information and perform the api-request again
    #
    def get_missing_information(self, no_info, missing_info, misunderstood_info = None):
        info = ''

        if missing_info == MissingInfoType.departure_station:
            info = 'departure station'
        elif missing_info == MissingInfoType.arrival_station:
            info = 'arrival station'
        else:
            self.speak_dialog('error with info type')

        if no_info:
            info = self.get_response('What is your {}?'.format(info))
            return info
        else:
            correct_info = self.ask_yesno('Is {} your {}?'.fourmat(misunderstood_info, info))
            if correct_info == 'yes':
                return misunderstood_info
            elif correct_info == 'no':
                info = self.get_response('What is your {}?'.format(info))
                return info
            else:
                self.speak_dialog('Please anwser the question only with yes or no')
                return self.get_missing_information(no_info, missing_info, misunderstood_info)

    #Do an API request with the given information from the user. 
    #If everything is correct and the API returns the correct information it will return departure time.
    #It needs arrival_station and depature_station, time is optional.
    #It will also at an attempt if it tries to get the missing information, 
    #if teh function it is used 2 times without the a return to a bot, it wil return None.
    #So it doesn't loop in this function indefinitely
    def get_departure_time(self, arrival_station, departure_station, time = None):
        if(self.attempts > 1):
            return None

        self.attempts += 1

        if arrival_station == None and departure_station == None:
            arrival_station = self.get_missing_information(True, MissingInfoType.arrival_station)
            departure_station = self.get_missing_information(True, MissingInfoType.departure_station)
        elif departure_station == None:
            departure_station = self.get_missing_information(True, MissingInfoType.departure_station)
        elif arrival_station == None:
            arrival_station = self.get_missing_information(True, MissingInfoType.arrival_station) 
           
        rfc_time = time

        if time != None: 
            rfc_time = self.time_to_rfc(rfc_time)

        #Ocp-Apim-Subscription-Key = primary key NS-API from your NS-API account
        headers = {
        'Authorization': '',
        'Ocp-Apim-Subscription-Key': '17b1f2bd9fcb40e686584a4022360477',
        }

        #datetime needs time in rfc to understand request
        params = urllib.parse.urlencode({

            'fromStation': '{}'.format(departure_station),
            'toStation': '{}'.format(arrival_station),
            'dateTime': '{}'.format(rfc_time)
        })

        try:
            conn = http.client.HTTPSConnection('gateway.apiportal.ns.nl')
            conn.request("GET", "/reisinformatie-api/api/v3/trips?%s" % params, "{body}", headers)
            response = conn.getresponse()
            data = response.read().decode('utf-8')
            json_obj = json.loads(data)

            if 'code' in json_obj:
                bad_request_code = json_obj['code']
                if bad_request_code == 400:
                    arrival_station = self.get_missing_information(False, MissingInfoType.arrival_station, arrival_station)
                    departure_station = self.get_missing_information(False, MissingInfoType.departure_station, departure_station)
                    return self.get_departure_time(arrival_station, departure_station, time)

            departure_time = self.check_departure_time(json_obj, time)      
            conn.close()
            return departure_time 

        #If there is an error while trying to get de data from teh api request,
        #it will return None so that the bot can give a respond that there is a problem    
        except Exception as e:
            return None

    #Get the time from de json data and return it as rfc
    def time_to_rfc(self, time):
        tz_NL = pytz.timezone('Europe/Amsterdam')
        current_time = datetime.now(tz_NL)
        rfc_3339_date = str(current_time).split(' ')[0]
        rfc_3339_time = rfc_3339_date + 'T' + time
        return rfc_3339_time

    #Get time to return als 'time' type
    def to_time_variable(self, time_var):
        hour = int(time_var.split(':')[0])
        minutes = int(time_var.split(':')[1])  
        return time(hour,minutes)

    #Sometimes it returns the departure time of a train that has already left the station
    #So this function checks if the time has already passed and if so it gets the next train and returns teh correct time
    def check_departure_time(self, data, chosen_time = None, train_count = 0): 
        
        if chosen_time == None:    
            tz_NL = pytz.timezone('Europe/Amsterdam')   
            current_time = datetime.now(tz_NL).time()
        else:
            current_time = chosen_time
            if isinstance(chosen_time, str): 
                current_time = self.to_time_variable(chosen_time)    

        arnu_obj = data['trips'][train_count]['uid']  
        departure_time = arnu_obj.split('|')[3].split('=')[1].split('T')[1].split('+')[0]       
        departure_time_int = self.to_time_variable(departure_time)
        self.platform = data['trips'][train_count]['legs'][0]['origin']['plannedTrack']
        
        if current_time < departure_time_int:  
            return departure_time
        else: 
            return self.check_departure_time(data, current_time, (train_count + 1))   


def create_skill():
    return TravelSkill()
